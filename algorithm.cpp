#include <cstdlib>
#include <iostream>
#include "Constants.h"
#include "algorithm.h"
#include <dirent.h>
#include <stdio.h>
#include <limits>
#include <fstream>
#include <sstream>

using namespace std;

void initGame(){
    resetConsole(GameConstants::WELCOME_TEXT);
    Quiz* quiz = chooseQuiz();
    runQuiz(quiz);
}

map<int, string>* listQuizOptions(){
    // znajdź pliki w folderze z testami
    map<int, string>* filesMap = checkDirectory(GameConstants::QUIZ_DIR);
    
    // wypisz zdefiniowane w folderze pliki
    map<int, string>::iterator it;
    cout << GameConstants::FOUND_TESTS<<endl;
    for(it = filesMap->begin(); it != filesMap->end(); it++){
        cout << it->first << ": " << it-> second<<endl; 
    }
    
    return filesMap;
}

Quiz* chooseQuiz(){
    map<int, string>* quizMap = listQuizOptions();
    cout << GameConstants::CHOOSE_QUIZ << endl;
    int choice = getNumberOrQuit(quizMap->size(), &initGame);
    cout << GameConstants::CHOSEN << quizMap->at(choice) << endl;
    moveForwardOrQuit(GameConstants::MOVE_TO_QUIZ, &initGame);
    Quiz* quiz = new Quiz();
    quiz->name = quizMap->at(choice);
    fstream file (GameConstants::QUIZ_DIR + quizMap->at(choice));
    if(file.is_open()){
        file >> *quiz;
    } else {
        // handle error
    }
    delete quizMap;
    return quiz;
}

void runQuiz(Quiz* quiz){
    stringstream sstream;
    int qNum = 0, ansInd;
    for(vector<Question*>::iterator it = quiz->questions->begin(); it != quiz->questions->end(); ++it) {
        sstream << "Pytanie " << ++qNum << "/" << quiz->questions->size() << ":\n" << (*it)->getValue() << "\n\n";        
        char options [(*it)->getAnswers()->size()] = {};
        ansInd = 0;
        for(map<char, string>::iterator it2 = (*it)->getAnswers()->begin(); it2 != (*it)->getAnswers()->end(); ++it2){
            sstream << ((*it2).first )<<": " <<((*it2).second) << "\n";
            options[ansInd++] = (*it2).first;
        }
        
        resetConsole(sstream.str());
        sstream.str(string());
        sstream.clear();
        quiz->score += getAnswer(options, ansInd + 1, (*it)->getCorrectAnswer(), (*it)->getValue(), (*it)->getAnswers())? 1 : 0;
    }
    
    sstream << "Koniect testu " << quiz-> name << endl << "Twój wynik to " << quiz->score << " na " << quiz->questions->size() << endl;
    resetConsole(sstream.str().c_str());
    sstream.clear();
    moveForwardOrQuit(GameConstants::QUIT_OR_REPEAT, initGame);
    cleanMemory(quiz);
    initGame();
}

void logAnswer(string question, char key, string answer){
    ofstream file(GameConstants::LOG_FILE, std::ios_base::app | std::ios_base::out);
    file << question << ": [" << key << "] - " << answer << endl;
    file.close();
}


bool getAnswer(char options [], int optionLen, char answer, string question, map<char, string>* answers){
    char input;
    cin >> input;
    for(int i = 0; i < optionLen; i++){
        if(options[i] == input){
//            string tmp_a = answers->at(input);
            logAnswer(question, input, answers->at(input));
            return options[i] == answer;
        }
    }
    cout << GameConstants::INVALID_INPUT << endl;
    cin.clear();
    return getAnswer(options, optionLen, answer, question, answers);
}

map<int, string>* checkDirectory(const char* absolutePath){
    map<int, string>* files = new map<int, string>();
    DIR *dir;
    struct dirent *file;
    string tmpFileName;
    int index = 0;
    if ((dir = opendir (absolutePath)) != NULL) {
      while ((file = readdir (dir)) != NULL) {
          tmpFileName = file->d_name;
          if(tmpFileName.substr(tmpFileName.find_last_of(".") + 1) == GameConstants::VALID_EXT){
              (*files)[++index] = tmpFileName;
          }
      }
      closedir (dir);
    } else {
        cout << GameConstants::DIR_ERROR << endl;
    }
    return files;
}


void resetConsole(const char * msg){
    system(SystemConstants::CLEAR);
    if(msg){
        cout << msg << endl;    
    }
}

void resetConsole(string msg){
    system(SystemConstants::CLEAR);
    if(!msg.empty()){
        cout << msg << endl;    
    }
}

void moveForwardOrQuit(const char * msg, void (*callback)()){
    if(msg){
        cout << msg << endl;
    }
    cin.ignore();
    char choice = cin.get();
    if(choice == GameConstants::QUIT_OPTION_CHAR){
        quitGame(callback);
    }
}

int getNumberOrQuit(int max, void (*callback)()){
    int input;
    while(true){
        cin >> input;
        if(cin.fail() || input < 0 || input > max) {
           cout << GameConstants::INVALID_INPUT << endl;
           cin.clear();
           cin.ignore(numeric_limits<streamsize>::max(), '\n'); // ciekawostal - bez tego infinite LOOP
        } else if(input == GameConstants::QUIT_OPTION){
           quitGame(callback);
        } else {
            return input;
        }
    } 
}

void quitGame(void (*callback)()){
    resetConsole(GameConstants::BEFORE_QUIT);
    char choice;
    while(true){
        cin >> choice;
        if(choice == 'y'){
            exit(0);
        } else if(choice == 'n'){
            callback();
        } else {
            quitGame(callback);
        }
    }
}

void cleanMemory(Quiz* quiz){
    
}