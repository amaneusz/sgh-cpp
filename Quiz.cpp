/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Quiz.cpp
 * Author: Jakub Amanowicz
 * 
 * Created on 15 listopada 2017, 11:33
 */

#include <iostream>
#include <fstream>
#include <cstring>

#include "Quiz.h"

Quiz::Quiz() {
    score = 0;
    questions = new vector<Question*>();
}

Quiz::Quiz(const Quiz& orig) {
}

Quiz::~Quiz() {
    if(questions != NULL){
        for(vector<Question*>::iterator it = questions->begin(); it != questions->end(); ++it){
            delete (*it);
        }
        questions->clear();
        delete questions;
    }
}

fstream& operator >> (fstream& is, Quiz& q){
    
    // pomijanie znaczników UTF-8
    char BOM[3] = {0};
    is.read(BOM, 3);
    if (
          !(
            (unsigned char)BOM[0] == 0xEF && 
            (unsigned char)BOM[1] == 0xBB && 
            (unsigned char)BOM[2] == 0xBF)
           ){
        is.seekg(0);
    }
    
    // faktyczny algorytm:
    string line;
        

    while(true){
        Question* current;
        char key;
        string value;
        
        // wczytanie i obsługa błędu strumieniowego || koniec streama
        if(!getline(is, line)){
            if(!current->isEmpty()){
             q.questions->push_back(current);   
            }
            break;
        }     
        
        if(line.empty() || (int) line[0] == 13){
            // nowe pytanie, idź dalej
            if(!current->isEmpty()){
             q.questions->push_back(current);   
            }
            continue;
        }
        
        key = line[0];
        value = line.substr(2);
        switch(key){
            case('Q'): {
                current = new Question();
                current->setValue(value);
                break;
            }
            case('R'): {
                current->setCorrectAnswer(value[0]);
                break;
            }
            default : {
                current->addAnswer(key, value);
                break;
            }
        }
        
    }
    
    return is;
}

