/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Quiz.h
 * Author: Jakub Amanowicz
 *
 * Created on 15 listopada 2017, 11:33
 */

#ifndef QUIZ_H
#define QUIZ_H
#include "Question.h"
#include <vector>
#include <fstream>

class Quiz {
public:
    Quiz();
    Quiz(const Quiz& orig);
    virtual ~Quiz();
    string name;
    int score;
    std::vector<Question*>* questions;
friend fstream& operator >> (fstream& is, Quiz& q);
 
};

#endif /* QUIZ_H */

