/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Constants.h
 * Author: Jakub Amanowicz
 *
 * Created on 15 listopada 2017, 12:00
 */

#ifndef CONSTANTS_H
#define CONSTANTS_H

namespace GameConstants {
    const char * WELCOME_TEXT = "Witaj w aplikacji testującej";
    const char * CHOOSE_QUIZ = "Wybierz quiz poprzez wprowadzenie wartości liczbowej";
    const char * BEFORE_QUIT = "Na pewno chcesz opuścić aplikację (y / n)?";
    const int QUIT_OPTION = 0;
    const int QUIT_OPTION_CHAR = '0';
    const char * INVALID_INPUT = "Wprowadzono nieprawidłową wartość. Spróbuj ponownie";
    const char * QUIZ_DIR = "C:\\Users\\Jakub Amanowicz\\Documents\\NetBeansProjects\\SGH Quiz\\testy\\";
//    const char * QUIZ_DIR = "C/Users/Jakub Amanowicz/Documents/NetBeansProjects/SGH Quiz/testy";
//    const char * QUIZ_DIR = "C:\\Users\\Jakub Amanowicz\\Documents\\NetBeansProjects\\SGH Quiz\\testy";
    const char * DIR_ERROR = "Wystąpił błąd z dostępem do ścieżki testowej";
    const char * VALID_EXT = "txt";
    const char * FOUND_TESTS = "Znaleziono następujące testy...";
    const char * CHOSEN = "Wybrano: ";
    const char * MOVE_TO_QUIZ = "Wciśnij dowolny klawisz by przejść dalej, lub 0, aby wyjść";
    const char * QUIT_OR_REPEAT = "Naciśnij 0 aby wyjść, inny klawisz, by powtórzyć";
    const char QUESTION_KEY = 'Q';
    const char RIGHT_ANS_KEY = 'R';
    
    // pod formaty
    const char * QUESTION_TEMPLATE = "";
    const char * RESULT_TEMPLATE = "";
    const char * SCORE_TEMPLATE = "";
 
    // pod logi
    const char * LOG_FILE = "logi.txt";
    
}

// poniższe podejście średnio bezpieczne: http://www.cplusplus.com/articles/j3wTURfi/ 
namespace SystemConstants {
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    const char * CLEAR = "cls";
#else
    const char * CLEAR = "clear";
#endif
}

#endif /* CONSTANTS_H */

