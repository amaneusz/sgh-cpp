/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Question.cpp
 * Author: Jakub Amanowicz
 * 
 * Created on 15 listopada 2017, 11:33
 */

#include "Question.h"

Question::Question() {
    answers = new map<char, string>();
}

Question::Question(const Question& orig) {
}

Question::~Question() {
    delete answers;
}


bool Question::isEmpty(){
    return answers->empty() || value.empty();
}

bool Question::isCorrect(char answer){
    return answer == correctAnswer;
}

void Question::setCorrectAnswer(const char& ans){
    correctAnswer = ans;
};

void Question::setValue(const string& val){
    value = val;
};

void Question::addAnswer(const char& key, const string& val){
    (*answers)[key] = val;
};

map<char, string>* Question::getAnswers(){
    return answers;
};

string Question::getValue(){
    return value;
};

char Question::getCorrectAnswer(){
    return correctAnswer;
};