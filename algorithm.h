/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   algorithm.h
 * Author: Jakub Amanowicz
 *
 * Created on 15 listopada 2017, 12:20
 */

#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "Quiz.h"

void initGame();
void resetConsole(const char* output);
void resetConsole(string output);
map<int, string>* listQuizOptions();
Quiz* chooseQuiz();
void runQuiz(Quiz* quiz);
void quitGame(void (*callback)(void));
map<int, string>* checkDirectory(const char* absolutePath);
int getNumberOrQuit(int max, void (*callback)(void));
void moveForwardOrQuit(const char * msg, void (*callback)());
bool getAnswer(char options [], int len, char answer, string question, map<char, string>* answers);
void cleanMemory(Quiz* quiz);
void logAnswer(string question, char key, string answer);

#endif /* ALGORITHM_H */

