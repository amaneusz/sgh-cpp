/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Question.h
 * Author: Jakub Amanowicz
 *
 * Created on 15 listopada 2017, 11:33
 */

#ifndef QUESTION_H
#define QUESTION_H
#include <map>

using namespace std;

class Question {
private:
    char correctAnswer;
    string value;
    map<char, string>* answers;
public:
    Question();
    Question(const Question& orig);
    virtual ~Question();
    bool isCorrect(char answer);
    bool isEmpty();
    void setCorrectAnswer(const char& ans);
    void setValue(const string& val);
    void addAnswer(const char& key, const string& val);
    map<char, string>* getAnswers();
    string getValue();
    char getCorrectAnswer();
};

#endif /* QUESTION_H */

